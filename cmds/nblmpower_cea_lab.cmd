# launch IOC in E3:
# iocsh.bash ./cmds/nblmpower.cmd

require caensyproxy, master

epicsEnvSet(P,              "$(P=CEA:)")        # default prefix is "CEA"
epicsEnvSet(Dis,            "$(Dis=PwrC)")
epicsEnvSet(CRATE_PREFIX,   "SY4527")

# CAEN crateZ
epicsEnvSet(Dev,            "PSSys")
epicsEnvSet(Idx,            "100")
epicsEnvSet(R,              "${Dis}-${Dev}-${Idx}:")
dbLoadRecords("CAEN_SY4527_crate.template", "P=${P}, R=${R}, CRATE_PREFIX="${CRATE_PREFIX}"")

## HV
epicsEnvSet(Dev,        "HVM")
epicsEnvSet(Idx,        "101")
epicsEnvSet(R,          "${Dis}-${Dev}-${Idx}:")
epicsEnvSet(HV_SLOT,    "05")
# board and its 48 channels
dbLoadRecords("CAEN_HV_A7030.db", "P=${P}, R=${R}, CRATE_PREFIX="${CRATE_PREFIX}", HV_SLOT="${HV_SLOT}",  EXT_TRIP="${EXT_TRIP=}"")

## LV
epicsEnvSet(Dev,        "LVM")
epicsEnvSet(Idx,        "102")
epicsEnvSet(R,          "${Dis}-${Dev}-${Idx}:")
epicsEnvSet(LV_SLOT,    "10")
# board and its 8 channels
dbLoadRecords("CAEN_LV_A2519.db", "P=${P}, R=${R}, CRATE_PREFIX="${CRATE_PREFIX}", LV_SLOT="${HV_SLOT}", INT_TRIP="${INT_TRIP=}", EXT_TRIP="${EXT_TRIP=}"")

iocInit()
dbl > pv.list
date

