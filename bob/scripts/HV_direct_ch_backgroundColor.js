importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var channelNumberString = PVUtil.getString(pvs[0])    // HV board channel number as tring (ex: "00" or "47")
//ConsoleUtil.writeInfo("arg: " + channelNumberString);

var channelNumber = parseInt(channelNumberString, 10)
// ConsoleUtil.writeInfo("arg int : " + parseInt(channelNumber));

// change backgreound color one channel over 2
if (channelNumber%2 == 0){
	widget.setPropertyValue("background_color", ColorFontUtil.getColorFromRGB(200,200,200));
}
else {
	widget.setPropertyValue("background_color", ColorFontUtil.getColorFromRGB(240,240,240));
}

