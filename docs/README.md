## CAEN trips

### internal trip (LV/HV board)
- 0x11: progagate and sensitine on trip line 0
- 0x22: progagate and sensitine on trip line 1
- 0x44: progagate and sensitine on trip line 2
- 0x88: progagate and sensitine on trip line 3

### external trip (crate)
- 0x10: progagate trip on CAEN rate backplane line 0
- 0x20: progagate trip on CAEN rate backplane line 1
- 0x40: progagate trip on CAEN rate backplane line 2
- 0x80: progagate trip on CAEN rate backplane line 3
