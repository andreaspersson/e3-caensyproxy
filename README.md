
# e3-caensyproxy

This module is related to CAEN SYx527 modular power supplies. These PSes have a built-in EPICS IOC running, which isn't modifiable. Thus this module has been developed as a short-term solution for naming compliance.

For more information, see [docs/README.md](docs/README.md).

## Requirements

- [e3](https://gitlab.esss.lu.se/e3/e3)

## Installation

```sh
$ make build
$ make install
```

For further targets, type `make`.

## Usage

```sh
$ iocsh.bash -r caensyproxy
```

In CSS the main opi to run is `power_supplies_SY4527_selection.opi`.

## Contributing

Contributions through pull/merge requests only.


